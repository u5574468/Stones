# Audit Landing Page-Week 10 Audit
* Documentation: [Google Drive](https://drive.google.com/drive/folders/0B81eEW0lWQa9aS1GdndnRVlrcmM?usp=sharing)
* Overview of the project: [Stones in the Cloud----Audit 3] (https://docs.google.com/document/d/1Lr_9VFe6eNXncEXnSstY8XyRD5KX6_ss6Vq5lBCj_08/edit)
* Project Audit 3 Slide: [Stones in the Cloud----Audit 3] (https://docs.google.com/presentation/d/1UMRoqYPAjBAZyyTjVtvn74QTFu4s5jm2XqPWGaXZIjE/edit#slide=id.p7)
* Task Schedule: [Taks Schedule](https://docs.google.com/document/d/1qCOf3LW8cjl5vZe5PcICGDPcUqeubUme8xqUI7a3WI0/edit)
* Decision Log: [Decision Log](https://docs.google.com/document/d/1wc7cNrFV6wM2i9bXqZVa6EvOpSb2rwkir7XByyH1dLw/edit)
* Risk Log: [Risk Log](https://docs.google.com/spreadsheets/d/1odjuaZgbeQkKeW405L1UQ_GyksWbYbg47Aa8ZeZtf_A/edit#gid=0)
* Stakeholder Engagement: [Stakeholder Engagement](https://drive.google.com/drive/folders/0B8fWWSXlu2bCMTFuNjZLb01ZNEk)
* Meeting Minutes: [Meeting Minutes](https://drive.google.com/drive/folders/1lcTkvn3kCxPQ0k6FUSxFkYKnV2BHALIZ)
* Website +New Server: [Stones in the Clouds](http://108.61.96.249/)


# Overview
The applicants propose to create a specialised database of information related to the enigmatic, megalithic jar sites of Laos, commonly known as the ‘Plain of Jars’. The Plain of Jars is currently the subject of an ANU-led Australian Research Council Discovery Project (DP 1501011164). The project comprises the collection of a substantial amount of data from excavations, remote sensing, and GIS applications and is crucial to the Lao People’s Democratic Republic efforts to have the Plain of Jars recognised as UNESCO World Heritage.

To date, excavation of two megalithic jar sites has been undertaken and four sites have been mapped and the morphological data of each jar and archaeologically important object at each recorded. The database will also incorporate data on the location and known condition of the 80+ other megalithic sites in Laos. The data produced by the ANU-led multi-institutional interdisciplinary research project will be used to bolster the Lao bid to have these jar sites listed as UNESCO World Heritage status and will also be valuable for the maintenance of these important sites once they are recognised as World Heritage. It is, therefore, crucial to create a flexible database that can be accessed from any computer in the world, allowing authorised parties to input, update and extract data for research and management purposes.

It is fortunate that heritage-specific software has been developed by the Getty Conservation Institute (GCI) and the World Monuments Fund aimed at improving the management of data for important heritage sites. The software, called ‘Arches’ is an open-source, geospatially-enabled software platform and fits perfectly the needs of the Plain of Jars project. As the GCI website explains;

“Arches incorporates widely adopted standards (for heritage inventories, heritage data, thesauri, and information technology) so that it will offer a solid foundation that heritage institutions may customize to meet their particular needs. Arches is built using open source software tools to make its adoption cost effective, and to allow heritage institutions to pool resources.”

The criteria for the database are that it is flexible enough to be able to accommodate varied data forms from GIS maps, drone video footage, 3D images, artefact photographs, textual data and be fully searchable and have the ability to cross reference. Another important aspect is that the database be web-based and accessible via a web-portal to authorised users in Laos (or elsewhere). This will allow the Lao to manage and expand the database into the future assisting them in preserving perhaps the most important heritage landscape in the country and allowing future researchers full access to the data produced by the current research project.

# Run our project 
There are two way run our project. 
*   Use our VM file
*   Install by your self

## Use our VM file
VM file:[VM](https://drive.google.com/open?id=19OVYJ2wJnaj_xPvgwTyO3d3SJ-tHZY_6k7Z2WoJmYDA)

## Install by yourself
### Installing Dependencies 
* Ubuntu:[Ubuntu](https://github.com/archesproject/arches/wiki/Installing-Dependencies-on-Ubuntu)
* macOS:[macOS](https://github.com/archesproject/arches/wiki/Installing-Dependencies-on-macOS)
* Windows:[Windows](https://github.com/archesproject/arches/wiki/Installing-Dependencies-on-Windows) 

### Git clone 
```
git clone git@gitlab.cecs.anu.edu.au:u5574468/Stones.git
cd Stones
```

### Create a virtual environmen
```
virtualenv ENV
```

### Activate virtual environment
Linux and macos:    
```
source ENV/bin/activate
```
Windows
```
ENV\Scripts\activate
```
### install arches
```
pip install arches --no-binary :all:
```
### [install  ElasticSearch](https://github.com/archesproject/arches/wiki/Installing-and-Running-Elasticsearch) 
### install db
```
python manage.py packages -o setup_db
```
### Run project
```
bash Run_UnixLike.sh
```
### Troubleshoot
| Traceback     | solution     |
| :------------- | :------------- |
| "Could not parse version info string "3.6.2-CAPI-1.10.2 4d2925d6"      | replace "ver = geos_version().decode()" to "ver = geos_version().decode().split(' ')[0]" in ENV/lib/python2.7/site-packages/django/contrib/gis/geos/libgeos.py      |
| "Failed to establish a new connection" | make sure your run ElasticSearch  |


