define(['knockout', 'underscore', 'viewmodels/widget'], function (ko, _, WidgetViewModel) {
    /**
    * registers a text-widget component for use in forms
    * @function external:"ko.components".text-widget
    * @param {object} params
    * @param {string} params.value - the value being managed
    * @param {function} params.config - observable containing config object
    * @param {string} params.config().label - label to use alongside the text input
    * @param {string} params.config().placeholder - default text to show in the text input
    */
    return ko.components.register('xyz-widget', {
        viewModel: function(params) {
            params.configKeys = ['x_placeholder','y_placeholder','z_placeholder'];
            WidgetViewModel.apply(this, [params]);
            var self = this;
	    // https://www.npmjs.com/package/ecef-projector
            var projector = require('ecef-projector');
	    
            if (this.value()) {
                var coords = this.value().split('POINT(')[1].replace(')','').split(' ')

                var gps = projector.unproject(coords[0], coords[1], coords[2]);
                this.x_value = ko.observable(gps[0]);
                this.y_value = ko.observable(gps[1]);
                this.z_value = ko.observable(gps[2]);

            } else {
                this.x_value = ko.observable();
                this.y_value = ko.observable();
                this.z_value = ko.observable();
            };

            this.preview = ko.pureComputed(function() {
                var res = "POINT(" + this.x_value() + " " + this.y_value() + " " + this.z_value() + ")"
                this.value(res);
                return res;
            }, this);
        },
        template: { require: 'text!templates/xyz-widget.htm' }
    });
});
